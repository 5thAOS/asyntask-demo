package com.example.rany.asyntaskdemo;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnAsyn;
    private ProgressDialog p_dialog;
    private int progress = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p_dialog = new ProgressDialog(this);
        p_dialog.setTitle(R.string.p_title);
        p_dialog.setMessage("Downloading...");
        p_dialog.setMax(100);
        p_dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        btnAsyn = findViewById(R.id.btnAsynTask);
        btnAsyn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyAsyntask().execute("testing");
            }
        });

    }

    class MyAsyntask extends AsyncTask<String, Integer, String>{

        // background thread
        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < 100 ; i++){
                try {
                    progress = i;
                    Thread.sleep(200);
                    publishProgress(progress);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return strings[0] + progress;
        }

        // UI thread
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p_dialog.show();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            p_dialog.dismiss();
            Toast.makeText(MainActivity.this, "Completed !", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            p_dialog.setProgress(values[0]);
        }
    }

}
