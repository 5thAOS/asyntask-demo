package com.example.rany.asyntaskdemo;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyntaskServerCom extends AppCompatActivity {

    private Button getData;
    private String result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asyntask_server_com);

        getData = findViewById(R.id.btnGetData);
        getData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyAsyn().execute("http://api-ams.me/v1/api/articles");
            }
        });
    }

    class MyAsyn extends AsyncTask<String, Integer, String>{

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                int c;
                while ((c = reader.read()) != -1){
                    result += String.valueOf((char)c);
                }
                Log.e("oooook", result+"");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("ooooo", "Result is : "+ result);
//            try {
//                JSONObject object = new JSONObject(result);
//                JSONArray data = object.getJSONArray("DATA");
//                data = data.getJSONObject("")
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("ooooo", "On PreExecute");
        }
    }

}
